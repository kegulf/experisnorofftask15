﻿using System;
using System.IO;

namespace ExperisNoroffTask15 {
    class Program {
        static void Main(string[] args) {

            // Creates a textfile in the SqlQueryFiles folder
            string baseURI = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            try { 
                StreamWriter file = new StreamWriter($"{baseURI}\\..\\SqlQueryFiles\\CharacterInsertSQL.txt");
                file.WriteLine("INSERT INTO Character(name, gender, classType) VALUES('DerpFace', 'Hermaphrodite', 'Mage')");

                file.Flush();

                file.Close();
            }
            catch(Exception ex) {
                Console.WriteLine(ex.GetType().Name);
                Console.WriteLine(ex.Message);
            }


        }
    }
}
